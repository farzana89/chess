# Chess

[![Node: 8.11.2](https://img.shields.io/badge/node-8.11.2-blue.svg)]()
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![coverage](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)]()

A basic chess game.
Game ends with check mate.

## Getting started

Run:
`yarn start`

Test:
`yarn test`

### Curretly unsupported feaures

* Scoring
* Castling
* En-passant
* Stale Mate
