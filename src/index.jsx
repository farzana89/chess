import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const chessApp = document.getElementById('chess-app');

function render(Component) {
  ReactDOM.render(<Component />, chessApp);
}
render(App);

if (module.hot) {
  module.hot.accept(() => {
    // eslint-disable-next-line global-require
    const newApp = require('./App').default;
    render(newApp);
  });
}
