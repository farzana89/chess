import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import App from './App';
import Board from './Board';
import Desk from './Desk';
import GameLog from './GameLog';
import PromotionModal from './PromotionModal';
import EndGameModal from './EndGameModal';

const renderer = new ShallowRenderer();

describe('Game startup tests', () => {
  it('App renders with all expected components', () => {
    renderer.render(<App />);
    const result = renderer.getRenderOutput();

    expect(result.props.children).toEqual([
      <Board />,
      <Desk />,
      <GameLog />,
      <PromotionModal />,
      <EndGameModal />
    ]);
  });
});
