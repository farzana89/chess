import React from 'react';
import { GameContext } from './Game';
import iconFactory from './images/iconFactory';

function Desk() {
  return (
    <div className="desk">
      <GameContext.Consumer>
        {context => (
          <React.Fragment>
            <p key="turn" className="turnLabel">
              <span className={context.state.Turn}>{context.state.Turn}</span>
            </p>
            {context.state.IsInCheck && (
              <p key="check" className="checkLabel">
                <span> Check</span>
              </p>
            )}
            <p key="info" className="infoLabel">
              <span> {context.state.InfoLabel}</span>
            </p>
            {context.state.capturedPieces.map((piece, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <div key={`D${index}`}>{iconFactory(piece)}</div>
            ))}
          </React.Fragment>
        )}
      </GameContext.Consumer>
    </div>
  );
}

export default Desk;
