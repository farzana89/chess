import React from 'react';
import PropTypes from 'prop-types';
import { DropTarget } from 'react-dnd';

const tileTarget = {
  drop(props) {
    return props.coords;
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

class Tile extends React.Component {
  render() {
    const {
      coords,
      children,
      isHighlighted,
      connectDropTarget,
      isOver
    } = this.props;

    return connectDropTarget(
      <div
        id={`${coords.x}.${coords.y}`}
        className={`tile ${isHighlighted ? 'highlight-tile' : ''} ${
          isOver ? 'highlight-move' : ''
        }`}
      >
        {children === null ? `${coords.x},${coords.y}` : children}
      </div>
    );
  }
}

const pointShape = PropTypes.shape({
  x: PropTypes.int,
  y: PropTypes.int
});

Tile.propTypes = {
  coords: pointShape.isRequired,
  children: PropTypes.node.isRequired,
  isHighlighted: PropTypes.bool,
  connectDropTarget: PropTypes.func.isRequired,
  isOver: PropTypes.bool.isRequired
};

Tile.defaultProps = {
  isHighlighted: false
};

export default DropTarget('Piece', tileTarget, collect)(Tile);
