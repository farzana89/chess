import TestRenderer from 'react-test-renderer';
import React from 'react';
import Desk from './Desk';
import PawnIcon from './images/PawnIcon';
import { setState } from './__mocks__/contextSetup';
import constructPiece from './PieceFactory';

const state = {
  turn: 'black',
  capturedPieces: [constructPiece({ colour: 'black', type: 'pawn' })]
};

setState(state);

jest.mock('./Game');

describe('Desk tests', () => {
  const renderer = TestRenderer.create(<Desk />);
  const instance = renderer.root;

  it('Renders turn label', () => {
    expect(
      instance
        .findByProps({ className: 'turnLabel' })
        .children.filter(x => x.type === 'span')
    ).not.toBeNull();
  });

  it('Renders svg icon for all captured pieces', () => {
    expect(instance.findByType(PawnIcon)).not.toBeNull();
  });
});
