import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import React from 'react';
import Tile from './Tile';
import Piece from './Piece';
import { GameContext } from './Game';
import config from './config';

function mapRows(context, row, rowNum) {
  return [
    <div key="rowNum" className="tileNumbers">
      {rowNum}
    </div>,

    row.map(item => {
      const tile = item[1];
      const piece = tile.content;
      let content = '';

      if (tile.content !== '') {
        content = (
          <Piece
            piece={piece}
            handleHoverIn={() => {
              context.handleHoverIn(piece, tile.coord);
            }}
            handleHoverOut={() => {
              context.handleHoverOut(piece, tile.coord);
            }}
            onMove={destCoord => {
              context.movePiece(tile.coord, piece, destCoord);
            }}
          />
        );
      }

      return (
        <Tile
          key={tile.coord.x + tile.coord.y}
          coords={tile.coord}
          isHighlighted={tile.isHighlighted}
        >
          {content}
        </Tile>
      );
    })
  ];
}

function renderRow(context, row, rowNum) {
  return (
    <div key={row[0][0]} className="board-row">
      {mapRows(context, row, rowNum)}
    </div>
  );
}

function renderBoard(context) {
  const board = Object.entries(context.state.tiles);
  const rows = [];
  const boardMax = config.boardDimension;
  for (let i = 0; i < boardMax; i++) {
    const skip = i * boardMax;
    const take = i * boardMax + boardMax;
    const rowNum = config.yAxisLabels[i];
    rows.push(renderRow(context, board.slice(skip, take), rowNum));
  }
  rows.push(
    <div key="letters" className="label-row">
      {config.xAxisLabels.map(item => (
        <div key={item} className="tileLabel">
          {item}
        </div>
      ))}
    </div>
  );
  return rows;
}

// DragDropContext expects a class
// eslint-disable-next-line react/prefer-stateless-function
class Board extends React.Component {
  render() {
    return (
      <GameContext.Consumer>
        {context => (
          <div key="board" className="board">
            <div className="status" />
            {renderBoard(context)}
          </div>
        )}
      </GameContext.Consumer>
    );
  }
}

export default DragDropContext(HTML5Backend)(Board);
