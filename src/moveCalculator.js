import Point from './Point';

export function getDiagonalCoords(maxMoves) {
  const path1 = [];
  const path2 = [];
  const path3 = [];
  const path4 = [];

  const xIncrement = 1;
  const yIncrement = 1;
  for (let i = 0; i < maxMoves; i++) {
    path1.push(Point(i + xIncrement, i + yIncrement));
    path2.push(Point(i + xIncrement, -i - yIncrement));
    path3.push(Point(-i - xIncrement, i + yIncrement));
    path4.push(Point(-i - xIncrement, -i - yIncrement));
  }
  return [path1, path2, path3, path4];
}

export function getVerticalCoords(maxMoves) {
  const path1 = [];
  const path2 = [];
  const yIncrement = 1;
  for (let i = 0; i < maxMoves; i++) {
    path1.push(Point(0, i + yIncrement));
    path2.push(Point(0, -i - yIncrement));
  }
  return [path1, path2];
}

export function getHorizontalCoords(maxMoves) {
  const path1 = [];
  const path2 = [];
  const xIncrement = 1;
  for (let i = 0; i < maxMoves; i++) {
    path1.push(Point(i + xIncrement, 0));
    path2.push(Point(-i - xIncrement, 0));
  }
  return [path1, path2];
}
