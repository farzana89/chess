export const context = {
  state: {
    tiles: []
  },
  movePiece: jest.fn(),
  handleHoverIn: jest.fn(),
  handleHoverOut: jest.fn()
};

export function setState(newState) {
  context.state = newState;
}
