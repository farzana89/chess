import { context } from './contextSetup';

const game = jest.genMockFromModule('../Game.jsx');

game.GameContext = {
  Consumer(props) {
    return props.children(context);
  }
};

module.exports = game;
