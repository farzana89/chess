import React from 'react';
import { GameContext } from './Game';
import config from './config';

function formatMoveLog(colour, type, srcCoord, destCoord) {
  return `${colour} ${type} moved from ${config.xAxisLabels[srcCoord.x]}${
    config.yAxisLabels[srcCoord.y]
  } to ${config.xAxisLabels[destCoord.x]}${config.yAxisLabels[destCoord.y]}`;
}

function formatCaptureLog(colour, type, destColour, destType, destCoord) {
  return `${colour} ${type} captured ${destColour} ${destType} at ${
    config.xAxisLabels[destCoord.x]
  }${config.yAxisLabels[destCoord.y]}`;
}

function GameLog() {
  return (
    <GameContext.Consumer>
      {context => {
        if (context.state.moveLog.length > 0) {
          return (
            <div className="gameLog">
              {context.state.moveLog
                .slice()
                .reverse()
                .map((move, index) => {
                  if (move.action === 'moved') {
                    return (
                      // eslint-disable-next-line react/no-array-index-key
                      <p key={`L${index}`}>
                        {formatMoveLog(
                          move.piece.colour,
                          move.piece.type,
                          move.srcCoord,
                          move.destCoord
                        )}
                      </p>
                    );
                  } else if (move.action === 'captured') {
                    return (
                      // eslint-disable-next-line react/no-array-index-key
                      <p key={`L${index}`}>
                        {formatCaptureLog(
                          move.piece.colour,
                          move.piece.type,
                          move.destContent.colour,
                          move.destContent.type,
                          move.destCoord
                        )}
                      </p>
                    );
                  }
                  return null;
                })}
            </div>
          );
        }
        return null;
      }}
    </GameContext.Consumer>
  );
}

export default GameLog;
