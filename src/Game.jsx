import React from 'react';
import PropTypes from 'prop-types';
import BoardController from './BoardController';

const GameContext = React.createContext();

class GameProvider extends React.Component {
  constructor() {
    super();
    this.controller = new BoardController();
    this.state = {
      Turn: this.controller.turn,
      IsInCheck: this.controller.check,
      IsPromoting: this.controller.IsPromoting,
      PromotingPiece: this.controller.PromotingPiece,
      InfoLabel: this.controller.infoLabel,
      tiles: this.controller.boardTiles,
      capturedPieces: this.controller.capturedPieces,
      moveLog: this.controller.moveLog,
      EndGame: this.controller.endGame
    };
  }

  updateState() {
    this.setState({
      Turn: this.controller.turn,
      IsInCheck: this.controller.check,
      tiles: this.controller.boardTiles,
      IsPromoting: this.controller.IsPromoting,
      PromotingPiece: this.controller.PromotingPiece,
      InfoLabel: this.controller.infoLabel,
      EndGame: this.controller.endGame
    });
  }

  render() {
    return (
      <GameContext.Provider
        value={{
          state: this.state,
          movePiece: (...args) => {
            this.controller.movePiece(...args);
            this.setState({
              capturedPieces: this.controller.capturedPieces
            });
            this.updateState();
          },
          handleHoverIn: (...args) => {
            this.controller.handleHoverIn(...args);
            this.updateState();
          },
          handleHoverOut: (...args) => {
            this.controller.handleHoverOut(...args);
            this.updateState();
          },
          promotePiece: (...args) => {
            this.controller.promotePiece(...args);
            this.updateState();
          }
        }}
      >
        {this.props.children}
      </GameContext.Provider>
    );
  }
}

GameProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export { GameProvider, GameContext };
