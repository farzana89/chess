import TestRenderer from 'react-test-renderer';
import React from 'react';
import GameLog from './GameLog';
import { setState } from './__mocks__/contextSetup';
import constructPiece from './PieceFactory';
import Point from './Point';

const moveLog = [
  {
    piece: constructPiece({ colour: 'black', type: 'pawn' }),
    action: 'moved',
    srcCoord: Point(1, 0),
    destCoord: Point(3, 0),
    destContent: ''
  },

  {
    piece: constructPiece({ colour: 'black', type: 'pawn' }),
    action: 'captured',
    srcCoord: Point(1, 0),
    destCoord: Point(2, 1),
    destContent: constructPiece({ colour: 'white', type: 'pawn' })
  }
];

const state = {
  moveLog
};

setState(state);

jest.mock('./Game');

describe('Log tests', () => {
  const renderer = TestRenderer.create(<GameLog />);
  const instance = renderer.root;

  it('Renders log for moves made', () => {
    const paragraphs = instance
      .find(element => element.props.className === 'gameLog')
      .children.filter(x => x.type === 'p');
    const logEntry = paragraphs[1].children[0];
    expect(logEntry).toEqual('black pawn moved from B8 to D8');
  });

  it('Renders log for capture of enemy piece', () => {
    const paragraphs = instance
      .find(element => element.props.className === 'gameLog')
      .children.filter(x => x.type === 'p');
    const logEntry = paragraphs[0].children[0];
    expect(logEntry).toEqual('black pawn captured white pawn at C7');
  });
});
