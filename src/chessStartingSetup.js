import Point from './Point';
import constructPiece from './PieceFactory';
import config from './config';

function chessStart() {
  const startPieces = [];
  startPieces.push({
    coord: Point(0, 0),
    content: constructPiece({ colour: 'black', type: 'rook' })
  });
  startPieces.push({
    coord: Point(1, 0),
    content: constructPiece({ colour: 'black', type: 'knight' })
  });
  startPieces.push({
    coord: Point(2, 0),
    content: constructPiece({ colour: 'black', type: 'bishop' })
  });
  startPieces.push({
    coord: Point(3, 0),
    content: constructPiece({ colour: 'black', type: 'king' })
  });
  startPieces.push({
    coord: Point(4, 0),
    content: constructPiece({ colour: 'black', type: 'queen' })
  });
  startPieces.push({
    coord: Point(5, 0),
    content: constructPiece({ colour: 'black', type: 'bishop' })
  });
  startPieces.push({
    coord: Point(6, 0),
    content: constructPiece({ colour: 'black', type: 'knight' })
  });
  startPieces.push({
    coord: Point(7, 0),
    content: constructPiece({ colour: 'black', type: 'rook' })
  });
  for (let i = 0; i < config.boardDimension; i++) {
    startPieces.push({
      coord: Point(i, 1),
      content: constructPiece({ colour: 'black', type: 'pawn' })
    });
  }
  // white pieces
  startPieces.push({
    coord: Point(0, 7),
    content: constructPiece({ colour: 'white', type: 'rook' })
  });
  startPieces.push({
    coord: Point(1, 7),
    content: constructPiece({ colour: 'white', type: 'knight' })
  });
  startPieces.push({
    coord: Point(2, 7),
    content: constructPiece({ colour: 'white', type: 'bishop' })
  });
  startPieces.push({
    coord: Point(4, 7),
    content: constructPiece({ colour: 'white', type: 'king' })
  });
  startPieces.push({
    coord: Point(3, 7),
    content: constructPiece({ colour: 'white', type: 'queen' })
  });
  startPieces.push({
    coord: Point(5, 7),
    content: constructPiece({ colour: 'white', type: 'bishop' })
  });
  startPieces.push({
    coord: Point(6, 7),
    content: constructPiece({ colour: 'white', type: 'knight' })
  });
  startPieces.push({
    coord: Point(7, 7),
    content: constructPiece({ colour: 'white', type: 'rook' })
  });

  for (let i = 0; i < config.boardDimension; i++) {
    startPieces.push({
      coord: Point(i, 6),
      content: constructPiece({ colour: 'white', type: 'pawn' })
    });
  }
  return startPieces;
}

export default chessStart;
