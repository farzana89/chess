import Point from './Point';
import {
  getDiagonalCoords,
  getVerticalCoords,
  getHorizontalCoords
} from './moveCalculator';

describe('Diagonal coords for moves', () => {
  it('Gets relative diagonal coords for a point which can move only one space', () => {
    const relativeMoves = getDiagonalCoords(1);

    expect(relativeMoves.length).toBe(4);
    const moves = [];
    relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
    expect(moves).toEqual([
      Point(1, 1),
      Point(1, -1),
      Point(-1, 1),
      Point(-1, -1)
    ]);
  });

  it('Gets relative diagonal coords for a point which can move 2 spaces', () => {
    const relativeMoves = getDiagonalCoords(2);
    expect(relativeMoves.length).toBe(4);
    const moves = [];
    relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
    expect(moves).toEqual([
      Point(1, 1),
      Point(2, 2),
      Point(1, -1),
      Point(2, -2),
      Point(-1, 1),
      Point(-2, 2),
      Point(-1, -1),
      Point(-2, -2)
    ]);
  });
});

describe('Vertical coords for moves', () => {
  it('Gets vertical coords for a point which can move only one space', () => {
    const relativeMoves = getVerticalCoords(1);
    const moves = [];
    relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
    expect(relativeMoves.length).toBe(2);
    expect(moves).toEqual([Point(0, 1), Point(0, -1)]);
  });

  it('Gets vertical coords for a point which can move 2 spaces', () => {
    const relativeMoves = getVerticalCoords(2);
    const moves = [];
    relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
    expect(relativeMoves.length).toBe(2);
    expect(moves).toEqual([
      Point(0, 1),
      Point(0, 2),
      Point(0, -1),
      Point(0, -2)
    ]);
  });
});

describe('Horizontal coords for moves', () => {
  it('Gets horizontal coords for a point which can move only one space', () => {
    const relativeMoves = getHorizontalCoords(1);
    const moves = [];
    relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
    expect(relativeMoves.length).toBe(2);
    expect(moves).toEqual([Point(1, 0), Point(-1, 0)]);
  });

  it('Gets horizontal coords for a point which can move 2 spaces', () => {
    const relativeMoves = getHorizontalCoords(2);
    const moves = [];
    relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
    expect(relativeMoves.length).toBe(2);
    expect(moves).toEqual([
      Point(1, 0),
      Point(2, 0),
      Point(-1, 0),
      Point(-2, 0)
    ]);
  });
});
