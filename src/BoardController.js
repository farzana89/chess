import Point from './Point';
import chessStart from './chessStartingSetup';
import { canBePromoted } from './Rules/Promotion';
import { getAvailMoves } from './Rules/Move';
import { getAttackMoves } from './Rules/Attack';
import { Check, CheckMate, MoveWillResultInCheck } from './Rules/Check';
import config from './config';

function setupTiles() {
  const tiles = {};
  for (let y = 0; y < config.boardDimension; y++) {
    for (let x = 0; x < config.boardDimension; x++) {
      const key = `${x}.${y}`;
      tiles[key] = { coord: Point(x, y), content: '' };
    }
  }
  return tiles;
}

function getStartingPieces() {
  return chessStart();
}

function isValid(moves, coord) {
  return moves.find(move => move.x === coord.x && move.y === coord.y);
}

const getValidMoves = (turnColour, currLoc, piece, boardTiles) => [
  ...getAvailMoves(turnColour, currLoc, piece, boardTiles),
  ...getAttackMoves(turnColour, currLoc, piece, boardTiles)
];

class BoardController {
  constructor() {
    this.turn = 'white';
    this.capturedPieces = [];
    this.moveLog = [];
    this.check = false;
    this.IsPromoting = false;
    this.PromotingPiece = '';
    this.infoLabel = '';
    this.boardTiles = setupTiles();
    this.endGame = false;
    this.setupStartupPieces();
  }

  setupStartupPieces() {
    const startMap = getStartingPieces();
    let i;
    for (i = 0; i < startMap.length; i++) {
      const piece = startMap[i];
      const key = `${piece.coord.x}.${piece.coord.y}`;
      this.boardTiles[key] = piece;
    }
  }

  highlightTile(coords) {
    this.boardTiles[`${coords.x}.${coords.y}`].isHighlighted = true;
  }

  removeHighlight(coords) {
    this.boardTiles[`${coords.x}.${coords.y}`].isHighlighted = false;
  }

  handleHoverIn(piece, coords) {
    const validMoves = getValidMoves(this.turn, coords, piece, this.boardTiles);

    validMoves.forEach(valid => {
      this.highlightTile(valid);
    });
  }

  handleHoverOut(piece, coords) {
    const validMoves = getValidMoves(this.turn, coords, piece, this.boardTiles);

    validMoves.forEach(valid => {
      this.removeHighlight(valid);
    });
  }

  handlePostMoveEffects(piece, destCoord) {
    if (canBePromoted(piece, destCoord)) {
      this.IsPromoting = true;
      this.PromotingPiece = { piece, destCoord };
    }
    if (piece.hasMoved) {
      piece.hasMoved();
    }

    const result = Check(this.boardTiles);

    this.check = result.isInCheck;

    this.changeTurn(result);
  }

  movePutsKingAtRisk(piece, srcCoord, destCoord) {
    const result = MoveWillResultInCheck(
      this.boardTiles,
      piece,
      srcCoord,
      destCoord
    );

    if (result.isInCheck && result.colourInCheck === this.turn) {
      return true;
    }
    return false;
  }

  changeTurn(result) {
    if (result.isInCheck) {
      this.turn = result.colourInCheck;
    } else {
      this.turn = this.turn === 'white' ? 'black' : 'white';
    }
    const checkResult = CheckMate(this.boardTiles);
    if (checkResult) {
      this.endGame = true;
    }
  }

  movePiece(srcCoord, piece, destCoord) {
    let action = 'moved';
    const destKey = `${destCoord.x}.${destCoord.y}`;
    const destTile = this.boardTiles[destKey];
    this.infoLabel = '';

    const destTileIsEmpty = destTile.content === '';

    const moves = destTileIsEmpty
      ? getValidMoves(this.turn, srcCoord, piece, this.boardTiles)
      : getAttackMoves(this.turn, srcCoord, piece, this.boardTiles);

    if (isValid(moves, destCoord)) {
      if (!this.movePutsKingAtRisk(piece, srcCoord, destCoord)) {
        action = !destTileIsEmpty ? 'captured' : 'moved';
        if (!destTileIsEmpty) this.capturePiece(destCoord);
      } else {
        this.infoLabel = this.check
          ? 'Move not allowed. King is at risk.'
          : 'Move not allowed. Will put king at risk';
        return;
      }
    } else {
      return;
    }

    this.handleHoverOut(piece, srcCoord);
    this.clearSourceTile(srcCoord);
    this.updateTileContent(destCoord, piece);
    this.handlePostMoveEffects(piece, Point(destCoord.x, destCoord.y));
    this.logMove(piece, action, srcCoord, destCoord, destTile.content);
  }

  logMove(piece, action, srcCoord, destCoord, destContent) {
    this.moveLog.push({
      piece,
      action,
      srcCoord,
      destCoord,
      destContent
    });
  }

  clearSourceTile(srcCoord) {
    const srcKey = `${srcCoord.x}.${srcCoord.y}`;
    this.boardTiles[srcKey] = {
      coord: Point(srcCoord.x, srcCoord.y),
      content: ''
    };
  }

  updateTileContent(destCoord, piece) {
    const destKey = `${destCoord.x}.${destCoord.y}`;
    this.boardTiles[destKey] = {
      coord: Point(destCoord.x, destCoord.y),
      content: piece
    };
  }

  capturePiece(destCoord) {
    const destKey = `${destCoord.x}.${destCoord.y}`;
    const destPiece = this.boardTiles[destKey];

    this.capturedPieces.push(destPiece.content);
  }

  promotePiece(piece) {
    this.updateTileContent(this.PromotingPiece.destCoord, piece);
    this.IsPromoting = false;
    this.PromotingPiece = '';
  }
}

export default BoardController;
