import Point from './Point';
import Pawn from './Pieces/Pawn';
import Queen from './Pieces/Queen';
import Bishop from './Pieces/Bishop';
import BoardController from './BoardController';
import Rook from './Pieces/Rook';
import Knight from './Pieces/Knight';

let board = null;

beforeEach(() => {
  board = new BoardController();
});

describe('Moving pieces', () => {
  it('should update board state when valid move is made', () => {
    const piece = new Rook('black');
    board.turn = 'black';
    board.movePiece(Point(2, 1), new Rook('black'), Point(2, 3));

    expect(board.boardTiles[2.3].content).toEqual(piece);
  });

  it('should not update board state for diagonal only piece attempts vertical move', () => {
    board.turn = 'black';
    board.movePiece(Point(2, 2), new Bishop('black'), Point(3, 2));

    expect(board.boardTiles[3.2].content).toEqual('');
  });
});

describe('Attacking enemy pieces', () => {
  it('should store piece once captured', () => {
    const destPiece = new Pawn('white');
    board.boardTiles[1.3] = {
      coord: Point(1, 3),
      content: destPiece
    };
    board.turn = 'black';
    board.movePiece(Point(1, 2), new Rook('black'), Point(1, 3));

    expect(board.capturedPieces).toContain(destPiece);
  });

  it('should not defeat piece if move is invalid', () => {
    const destPiece = new Pawn('white');
    board.boardTiles[1.3] = {
      coord: Point(1, 3),
      content: destPiece
    };
    board.turn = 'black';
    board.movePiece(Point(1, 4), new Pawn('black'), Point(1, 3));

    expect(board.capturedPieces).not.toContain(destPiece);
  });
});

describe('Pawn promotion', () => {
  it('Should update board state to promoting when a move results in pawn promotion', () => {
    const pawn = new Pawn('black');
    const destination = Point(4, 7);
    board.turn = 'black';

    board.movePiece(Point(3, 6), pawn, destination);

    expect(board.IsPromoting).toBe(true);

    expect(board.PromotingPiece).toEqual({
      destCoord: destination,
      piece: pawn
    });
  });

  it('Should update board state to not promoting once promotion is complete', () => {
    const pawn = new Pawn('black');
    const currLoc = Point(4, 7);
    const locKey = `${currLoc.x}.${currLoc.y}`;
    board.turn = 'black';

    board.boardTiles[locKey].content = pawn;

    board.IsPromoting = true;
    board.PromotingPiece = {
      destCoord: currLoc,
      piece: pawn
    };

    board.promotePiece(new Knight('black'));

    expect(board.IsPromoting).toBe(false);
    expect(board.PromotingPiece).toBe('');
  });

  it('Should replace pawn with selected promotion piece', () => {
    const currLoc = Point(4, 7);
    const locKey = `${currLoc.x}.${currLoc.y}`;
    board.turn = 'black';

    const pawn = new Pawn('black');
    board.boardTiles[locKey].content = pawn;

    board.IsPromoting = true;
    board.PromotingPiece = {
      destCoord: currLoc,
      piece: pawn
    };

    const promotionPiece = new Queen('black');
    board.promotePiece(promotionPiece);

    expect(board.boardTiles[locKey].content).toBe(promotionPiece);
  });
});
