import Queen from './Queen';
import Point from '../Point';

function expectedVerticalMoves() {
  return [
    Point(0, 1),
    Point(0, 2),
    Point(0, 3),
    Point(0, 4),
    Point(0, 5),
    Point(0, 6),
    Point(0, 7),
    Point(0, 8),
    Point(0, -1),
    Point(0, -2),
    Point(0, -3),
    Point(0, -4),
    Point(0, -5),
    Point(0, -6),
    Point(0, -7),
    Point(0, -8)
  ];
}
function expectedHorizontalMoves() {
  return [
    Point(1, 0),
    Point(2, 0),
    Point(3, 0),
    Point(4, 0),
    Point(5, 0),
    Point(6, 0),
    Point(7, 0),
    Point(8, 0),
    Point(-1, 0),
    Point(-2, 0),
    Point(-3, 0),
    Point(-4, 0),
    Point(-5, 0),
    Point(-6, 0),
    Point(-7, 0),
    Point(-8, 0)
  ];
}
function expectedDiagonalMoves() {
  return [
    Point(1, 1),
    Point(2, 2),
    Point(3, 3),
    Point(4, 4),
    Point(5, 5),
    Point(6, 6),
    Point(7, 7),
    Point(8, 8),
    Point(1, -1),
    Point(2, -2),
    Point(3, -3),
    Point(4, -4),
    Point(5, -5),
    Point(6, -6),
    Point(7, -7),
    Point(8, -8),
    Point(-1, 1),
    Point(-2, 2),
    Point(-3, 3),
    Point(-4, 4),
    Point(-5, 5),
    Point(-6, 6),
    Point(-7, 7),
    Point(-8, 8),
    Point(-1, -1),
    Point(-2, -2),
    Point(-3, -3),
    Point(-4, -4),
    Point(-5, -5),
    Point(-6, -6),
    Point(-7, -7),
    Point(-8, -8)
  ];
}

it('Gets valid moves for queen on initialization', () => {
  const queen = new Queen('black');

  const moves = [];
  const relativeMoves = queen.moveCoords;
  relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
  expect(relativeMoves.length).toBe(8);

  expect(moves).toEqual([
    ...expectedVerticalMoves(),
    ...expectedHorizontalMoves(),
    ...expectedDiagonalMoves()
  ]);
});
