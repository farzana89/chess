import { getDiagonalCoords } from '../moveCalculator';
import config from '../config';

function getBishopDiagonalCoords() {
  const maxBishopMoves = config.boardDimension;
  return getDiagonalCoords(maxBishopMoves);
}

class Bishop {
  constructor(colour) {
    this.type = 'bishop';
    this.colour = colour;
    this.canJump = false;
    this.moveCoords = getBishopDiagonalCoords();
    this.attackCoords = this.moveCoords;
  }
}

export default Bishop;
