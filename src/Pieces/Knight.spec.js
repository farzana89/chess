import Point from '../Point';
import Knight from './Knight';

it('Gets valid moves for knight on intialization', () => {
  const knight = new Knight('black');

  const moves = [];
  const relativeMoves = knight.moveCoords;
  relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
  expect(relativeMoves.length).toBe(8);

  expect(moves).toEqual([
    Point(1, 2),
    Point(1, -2),
    Point(-1, 2),
    Point(-1, -2),
    Point(2, 1),
    Point(2, -1),
    Point(-2, 1),
    Point(-2, -1)
  ]);
});
