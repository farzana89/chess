import {
  getHorizontalCoords,
  getVerticalCoords,
  getDiagonalCoords
} from '../moveCalculator';
import config from '../config';

function getQueenCoords() {
  const maxQueenMoves = config.boardDimension;
  return [
    ...getVerticalCoords(maxQueenMoves),
    ...getHorizontalCoords(maxQueenMoves),
    ...getDiagonalCoords(maxQueenMoves)
  ];
}

class Queen {
  constructor(colour) {
    this.type = 'queen';
    this.colour = colour;
    this.canJump = false;
    this.moveCoords = getQueenCoords();
    this.attackCoords = this.moveCoords;
  }
}

export default Queen;
