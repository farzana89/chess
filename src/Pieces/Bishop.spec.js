import Bishop from './Bishop';
import Point from '../Point';

it('Sets moveCoords for bishop on intialization', () => {
  const piece = new Bishop('black');
  const relativeMoves = piece.moveCoords;
  const moves = [];
  relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
  expect(relativeMoves.length).toBe(4);
  expect(moves).toEqual([
    Point(1, 1),
    Point(2, 2),
    Point(3, 3),
    Point(4, 4),
    Point(5, 5),
    Point(6, 6),
    Point(7, 7),
    Point(8, 8),
    Point(1, -1),
    Point(2, -2),
    Point(3, -3),
    Point(4, -4),
    Point(5, -5),
    Point(6, -6),
    Point(7, -7),
    Point(8, -8),
    Point(-1, 1),
    Point(-2, 2),
    Point(-3, 3),
    Point(-4, 4),
    Point(-5, 5),
    Point(-6, 6),
    Point(-7, 7),
    Point(-8, 8),
    Point(-1, -1),
    Point(-2, -2),
    Point(-3, -3),
    Point(-4, -4),
    Point(-5, -5),
    Point(-6, -6),
    Point(-7, -7),
    Point(-8, -8)
  ]);
});
