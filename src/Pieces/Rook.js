import { getHorizontalCoords, getVerticalCoords } from '../moveCalculator';
import config from '../config';

function getRookCoords() {
  return [
    ...getVerticalCoords(config.boardDimension),
    ...getHorizontalCoords(config.boardDimension)
  ];
}

class Rook {
  constructor(colour) {
    this.type = 'rook';
    this.colour = colour;
    this.canJump = false;
    this.moveCoords = getRookCoords();
    this.attackCoords = this.moveCoords;
  }
}

export default Rook;
