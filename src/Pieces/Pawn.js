import Point from '../Point';

const whiteMoves = [Point(0, -1), Point(0, -2)];
const whiteAttackMoves = [[Point(-1, -1)], [Point(1, -1)]];
const blackMoves = [Point(0, 1), Point(0, 2)];
const blackAttackMoves = [[Point(-1, 1)], [Point(1, 1)]];

class Pawn {
  constructor(colour) {
    this.type = 'pawn';
    this.colour = colour;
    this.canJump = false;
    this.moveCoords = [
      this.colour === 'black' ? blackMoves.slice() : whiteMoves.slice()
    ];
    this.attackCoords =
      this.colour === 'black'
        ? blackAttackMoves.slice()
        : whiteAttackMoves.slice();
  }

  hasMoved() {
    if (this.moveCoords[0].length > 1) {
      this.moveCoords[0].pop();
    }
  }
}

export default Pawn;
