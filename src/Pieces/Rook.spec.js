import Rook from './Rook';
import Point from '../Point';

function getExpected() {
  const expectedMoves = [
    Point(0, 1),
    Point(0, 2),
    Point(0, 3),
    Point(0, 4),
    Point(0, 5),
    Point(0, 6),
    Point(0, 7),
    Point(0, 8),
    Point(0, -1),
    Point(0, -2),
    Point(0, -3),
    Point(0, -4),
    Point(0, -5),
    Point(0, -6),
    Point(0, -7),
    Point(0, -8),
    Point(1, 0),
    Point(2, 0),
    Point(3, 0),
    Point(4, 0),
    Point(5, 0),
    Point(6, 0),
    Point(7, 0),
    Point(8, 0),
    Point(-1, 0),
    Point(-2, 0),
    Point(-3, 0),
    Point(-4, 0),
    Point(-5, 0),
    Point(-6, 0),
    Point(-7, 0),
    Point(-8, 0)
  ];

  return expectedMoves;
}

it('Gets valid moves for black rook on initialization', () => {
  const rook = new Rook('black');

  const moves = [];
  const relativeMoves = rook.moveCoords;
  relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
  expect(relativeMoves.length).toBe(4);
  expect(moves).toEqual(getExpected());
});
