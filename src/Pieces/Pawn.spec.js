import Pawn from './Pawn';
import Point from '../Point';

describe('Pawn move coords', () => {
  it('Gets valid moves for black pawn on initialization', () => {
    const pawn = new Pawn('black');
    const moves = [];
    const relativeMoves = pawn.moveCoords;
    relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
    expect(relativeMoves.length).toBe(1);

    expect(moves).toEqual([Point(0, 1), Point(0, 2)]);
  });

  it('Gets valid moves for white pawn on initialization', () => {
    const pawn = new Pawn('white');
    const moves = [];
    const relativeMoves = pawn.moveCoords;
    relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
    expect(relativeMoves.length).toBe(1);
    expect(moves).toEqual([Point(0, -1), Point(0, -2)]);
  });

  it('Ensures only one move is available after first move is made', () => {
    const pawn = new Pawn('white');

    pawn.hasMoved();
    const moves = [];
    const relativeMoves = pawn.moveCoords;
    relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
    expect(relativeMoves.length).toBe(1);
    expect(moves).toEqual([Point(0, -1)]);
  });
});

describe('Pawn attack coords', () => {
  it('Sets black pawn attack moves on initialization', () => {
    const pawn = new Pawn('black');
    const attackMoves = [];
    const relAttackMoves = pawn.attackCoords;
    relAttackMoves.forEach(path =>
      path.forEach(move => attackMoves.push(move))
    );
    expect(relAttackMoves.length).toBe(2);
    expect(attackMoves).toEqual([Point(-1, 1), Point(1, 1)]);
  });
});
