import {
  getHorizontalCoords,
  getVerticalCoords,
  getDiagonalCoords
} from '../moveCalculator';

function getKingCoords() {
  const maxKingMoves = 1;
  return [
    ...getVerticalCoords(maxKingMoves),
    ...getHorizontalCoords(maxKingMoves),
    ...getDiagonalCoords(maxKingMoves)
  ];
}

class King {
  constructor(colour) {
    this.type = 'king';
    this.colour = colour;
    this.canJump = false;
    this.moveCoords = getKingCoords();
    this.attackCoords = this.moveCoords;
  }
}

export default King;
