import King from './King';
import Point from '../Point';

it('Gets valid moves for black king on initialization', () => {
  const king = new King('black', 'king');
  const moves = [];
  const relativeMoves = king.moveCoords;
  relativeMoves.forEach(path => path.forEach(move => moves.push(move)));
  expect(relativeMoves.length).toBe(8);

  expect(moves).toEqual([
    Point(0, 1),
    Point(0, -1),
    Point(1, 0),
    Point(-1, 0),
    Point(1, 1),
    Point(1, -1),
    Point(-1, 1),
    Point(-1, -1)
  ]);
});
