import Point from '../Point';

const moveCoords = [
  // y options
  [new Point(1, 2)],
  [new Point(1, -2)],
  [new Point(-1, 2)],
  [new Point(-1, -2)],
  // x options
  [new Point(2, 1)],
  [new Point(2, -1)],
  [new Point(-2, 1)],
  [new Point(-2, -1)]
];

class Knight {
  constructor(colour) {
    this.type = 'knight';
    this.colour = colour;
    this.canJump = true;
    this.moveCoords = moveCoords;
    this.attackCoords = this.moveCoords;
  }
}

export default Knight;
