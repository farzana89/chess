import React from 'react';
import PropTypes from 'prop-types';
import { DragSource } from 'react-dnd';
import Pawn from './Pieces/Pawn';
import Knight from './Pieces/Knight';
import Bishop from './Pieces/Bishop';
import Rook from './Pieces/Rook';
import Queen from './Pieces/Queen';
import King from './Pieces/King';
import iconFactory from './images/iconFactory';

const pieceSource = {
  beginDrag(props) {
    return {
      text: props.piece
    };
  },
  endDrag(props, monitor) {
    if (monitor.didDrop()) {
      const result = monitor.getDropResult();
      props.onMove(result);
    }
  }
};

export const ItemTypes = {
  PIECE: 'Piece'
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

class Piece extends React.Component {
  render() {
    const {
      piece,
      handleHoverIn,
      handleHoverOut,
      isDragging,
      connectDragSource
    } = this.props;

    return connectDragSource(
      <div
        style={{
          opacity: isDragging ? 0.5 : 1,
          cursor: 'move'
        }}
      >
        {iconFactory(piece, handleHoverIn, handleHoverOut)}
      </div>
    );
  }
}
Piece.propTypes = {
  piece: PropTypes.oneOfType(
    [Pawn, Knight, Bishop, Rook, Queen, King].map(PropTypes.instanceOf)
  ).isRequired,
  handleHoverIn: PropTypes.func.isRequired,
  handleHoverOut: PropTypes.func.isRequired,
  isDragging: PropTypes.bool.isRequired,
  connectDragSource: PropTypes.func.isRequired
};

export default DragSource(ItemTypes.PIECE, pieceSource, collect)(Piece);
