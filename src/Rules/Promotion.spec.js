import Point from '../Point';
import constructPiece from '../PieceFactory';
import { canBePromoted, getPromotionOptions } from './Promotion';

describe('Pawn promotion rules', () => {
  it('Gets promotion options when pawn reaches opposing end of board', () => {
    const canPromote = canBePromoted(
      constructPiece({ type: 'pawn', colour: 'black' }),
      Point(1, 0)
    );
    expect(canPromote).toEqual(true);
  });

  it('Return false for pawn not reaching opposing end of board', () => {
    const canPromote = canBePromoted(
      constructPiece({ type: 'pawn', colour: 'black' }),
      Point(3, 3)
    );
    expect(canPromote).toEqual(false);
  });

  it('Return promotion options for pawn', () => {
    const promotionOptions = getPromotionOptions('black');
    expect(promotionOptions).toEqual([
      constructPiece({ colour: 'black', type: 'queen' }),
      constructPiece({ colour: 'black', type: 'knight' }),
      constructPiece({ colour: 'black', type: 'rook' }),
      constructPiece({ colour: 'black', type: 'bishop' })
    ]);
  });
});

describe('Invalid pieces for promotion', () => {
  it('Return false for knight promotion check', () => {
    const canPromote = canBePromoted('knight', Point(1, 0));
    expect(canPromote).toEqual(false);
  });
});
