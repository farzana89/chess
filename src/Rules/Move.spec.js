import Point from '../Point';
import Knight from '../Pieces/Knight';
import Pawn from '../Pieces/Pawn';
import Bishop from '../Pieces/Bishop';
import { setupTiles } from './TestSetup';
import { getAvailMoves } from './Move';

const boardTiles = setupTiles();

describe('Valid knight moves', () => {
  it('Gets valid moves for knight excluding locations with team piece', () => {
    boardTiles['4.1'] = {
      coord: Point(4, 1),
      content: new Pawn('black')
    };
    const knight = new Knight('black');
    const turnColour = 'black';
    const validMoves = getAvailMoves(
      turnColour,
      Point(6, 0),
      knight,
      boardTiles
    );
    expect(validMoves).toEqual([Point(7, 2), Point(5, 2)]);
  });

  it('Gets no valid moves for black knight when turn is currently for white piece', () => {
    const knight = new Knight('black');
    const turnColour = 'white';
    const validMoves = getAvailMoves(
      turnColour,
      Point(6, 0),
      knight,
      boardTiles
    );
    expect(validMoves).toEqual([]);
  });
});

describe('Valid pawn moves', () => {
  it('should get pawn moves', () => {
    const pawn = new Pawn('black');
    const turnColour = 'black';
    const validMoves = getAvailMoves(turnColour, Point(6, 1), pawn, boardTiles);
    expect(validMoves).toEqual([Point(6, 2), Point(6, 3)]);
  });
});

describe('Valid bishop moves', () => {
  it('should get no valid bishop moves', () => {
    const bishop = new Bishop('black');
    const turnColour = 'black';
    const validMoves = getAvailMoves(
      turnColour,
      Point(6, 1),
      bishop,
      boardTiles
    );
    expect(validMoves).toEqual([
      Point(7, 2),
      Point(7, 0),
      Point(5, 2),
      Point(4, 3),
      Point(3, 4),
      Point(2, 5),
      Point(1, 6),
      Point(0, 7),
      Point(5, 0)
    ]);
  });
});
