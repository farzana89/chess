import Point from '../Point';
import config from '../config';

const boardScope = [...Array(config.boardDimension).keys()];

function moveFitsBoardDimension(point) {
  return boardScope.includes(point.x) && boardScope.includes(point.y);
}

function moveContains(point, boardTiles, predicate) {
  const key = `${point.x}.${point.y}`;
  const tileContent = boardTiles[key].content;
  return predicate(tileContent);
}

export function sum(a, b) {
  return a + b;
}

function getBlockersForPath(currLoc, path, boardTiles) {
  const blockers = [];
  path.forEach(relMove => {
    const move = Point(sum(currLoc.x, relMove.x), sum(currLoc.y, relMove.y));
    if (
      moveContains(move, boardTiles, tileContent => tileContent !== '') &&
      blockers.length === 0
    ) {
      blockers.push(relMove);
    }
  });
  return blockers;
}

export function getAttackMoves(turnColour, currLoc, piece, boardTiles) {
  const attackMoves = [];
  piece.attackCoords.forEach(path => {
    const filteredPath = path.filter(point =>
      moveFitsBoardDimension(
        Point(sum(currLoc.x, point.x), sum(currLoc.y, point.y))
      )
    );
    const blockers = getBlockersForPath(currLoc, filteredPath, boardTiles);
    filteredPath.some(coord => {
      const move = Point(sum(currLoc.x, coord.x), sum(currLoc.y, coord.y));
      const isBlocked = blockers.find(
        blocker => blocker.x === coord.x && blocker.y === coord.y
      );
      if (
        piece.colour === turnColour &&
        isBlocked &&
        moveContains(
          move,
          boardTiles,
          tileContent => tileContent.colour !== piece.colour
        )
      ) {
        attackMoves.push(move);
        return true;
      }
      return false;
    });
  });
  return attackMoves;
}
