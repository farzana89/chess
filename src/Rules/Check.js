import { getAttackMoves } from './Attack';
import { getAvailMoves } from './Move';

export function IsInCheck(boardTiles, piece, pieceLoc, turn) {
  const attackMoves = getAttackMoves(turn, pieceLoc, piece, boardTiles);
  let check = false;
  attackMoves.forEach(move => {
    const moveKey = `${move.x}.${move.y}`;
    const tileContent = boardTiles[moveKey].content;
    if (tileContent.type === 'king') {
      check = true;
    }
  });
  return check;
}

export function Check(boardTiles) {
  const occupiedTiles = Object.values(boardTiles).filter(
    tile => tile.content !== ''
  );

  let isInCheck = false;
  let colourInCheck = '';

  // Look into ways to jump out of loop as soon as check is found
  occupiedTiles.forEach(tile => {
    const piece = tile.content;
    const checkResult = IsInCheck(boardTiles, piece, tile.coord, piece.colour);
    if (checkResult) {
      isInCheck = true;
      colourInCheck = piece.colour === 'black' ? 'white' : 'black';
    }
  });

  return {
    isInCheck,
    colourInCheck
  };
}

export function MoveWillResultInCheck(boardTiles, piece, srcLoc, destLoc) {
  const srcKey = `${srcLoc.x}.${srcLoc.y}`;
  const destKey = `${destLoc.x}.${destLoc.y}`;

  const tiles = JSON.parse(JSON.stringify(boardTiles));

  tiles[srcKey].content = '';
  tiles[destKey].content = piece;

  return Check(tiles);
}

function GetAvailMovesThatWillResolveCheck(boardTiles, turn) {
  const checkMoves = [];
  const pieces = Object.values(boardTiles).filter(
    x => x.content.colour === turn
  );

  pieces.forEach(tile => {
    const location = tile.coord;
    const piece = tile.content;
    const moves = [
      ...getAvailMoves(turn, location, piece, boardTiles),
      ...getAttackMoves(turn, tile.coord, piece, boardTiles)
    ];
    moves.forEach(move => {
      const result = MoveWillResultInCheck(boardTiles, piece, location, move);
      if (!result.isInCheck) {
        checkMoves.push({ piece, loc: move });
      }
    });
  });
  return checkMoves;
}

export function CheckMate(boardTiles) {
  const result = Check(boardTiles);

  if (result.isInCheck) {
    const checkColour = result.colourInCheck;

    const resolveCheckMoves = GetAvailMovesThatWillResolveCheck(
      boardTiles,
      checkColour
    );

    return resolveCheckMoves.length === 0;
  }
}
