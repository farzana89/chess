import config from '../config';
import Point from '../Point';

export function setupTiles() {
  const tiles = {};
  for (let y = 0; y < config.boardDimension; y++) {
    for (let x = 0; x < config.boardDimension; x++) {
      const key = `${x}.${y}`;
      tiles[key] = { coord: Point(x, y), content: '' };
    }
  }
  return tiles;
}

export function addPieceToTile(tiles, loc, piece) {
  const locKey = `${loc.x}.${loc.y}`;
  // eslint-disable-next-line no-param-reassign
  tiles[locKey] = {
    coord: loc,
    content: piece
  };

  return tiles;
}
