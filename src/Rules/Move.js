import Point from '../Point';
import config from '../config';

const boardScope = [...Array(config.boardDimension).keys()];

export function checkInclusive(element, coord) {
  const xIsNegative = Math.sign(coord.x) < 0 && Math.sign(element.x) < 0;
  const yIsNegative = Math.sign(coord.y) < 0 && Math.sign(element.y) < 0;
  return (
    (xIsNegative === true ? coord.x <= element.x : coord.x >= element.x) &&
    (yIsNegative === true ? coord.y <= element.y : coord.y >= element.y)
  );
}

function moveFitsBoardDimension(point) {
  return boardScope.includes(point.x) && boardScope.includes(point.y);
}

function moveContains(point, boardTiles, predicate) {
  const key = `${point.x}.${point.y}`;
  const tileContent = boardTiles[key].content;
  return predicate(tileContent);
}

function sum(a, b) {
  return a + b;
}

function getBlockersForPath(currLoc, path, boardTiles) {
  const blockers = [];
  path.forEach(relMove => {
    const move = Point(sum(currLoc.x, relMove.x), sum(currLoc.y, relMove.y));
    if (
      moveContains(move, boardTiles, tileContent => tileContent !== '') &&
      blockers.length === 0
    ) {
      blockers.push(relMove);
    }
  });
  return blockers;
}

export function getAvailMoves(turnColour, currLoc, piece, boardTiles) {
  const validMoves = [];
  piece.moveCoords.forEach(path =>
    path
      .filter(point =>
        moveFitsBoardDimension(
          Point(sum(currLoc.x, point.x), sum(currLoc.y, point.y))
        )
      )
      .forEach((coord, _, filteredPath) => {
        const blockers = getBlockersForPath(currLoc, filteredPath, boardTiles);
        const move = Point(sum(currLoc.x, coord.x), sum(currLoc.y, coord.y));
        if (moveContains(move, boardTiles, tileContent => tileContent === '')) {
          if (
            piece.colour === turnColour &&
            (piece.canJump || !blockers.some(x => checkInclusive(x, coord)))
          ) {
            validMoves.push(move);
          }
        }
      })
  );
  return validMoves;
}
