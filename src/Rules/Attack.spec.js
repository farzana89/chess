import Point from '../Point';
import Pawn from '../Pieces/Pawn';
import Rook from '../Pieces/Rook';
import { setupTiles, addPieceToTile } from './TestSetup';
import { getAttackMoves } from './Attack';

const boardTiles = setupTiles();

describe('Attacking enemy pieces', () => {
  it('should get valid attack moves for piece', () => {
    addPieceToTile(boardTiles, Point(1, 3), new Pawn('white'));
    const turnColour = 'black';

    const attackMoves = getAttackMoves(
      turnColour,
      Point(1, 2),
      new Rook('black'),
      boardTiles
    );

    expect(attackMoves).toContainEqual(Point(1, 3));
  });

  it('should not allow attack piece if move is invalid', () => {
    addPieceToTile(boardTiles, Point(1, 3), new Pawn('white'));

    const turnColour = 'black';
    const attackMoves = getAttackMoves(
      turnColour,
      Point(5, 6),
      new Rook('black'),
      boardTiles
    );

    expect(attackMoves).not.toContain([Point(1, 3)]);
  });
});
