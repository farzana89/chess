import constructPiece from '../PieceFactory';
import config from '../config';

const minBoardAxis = 0;
const maxBoardAxis = config.boardDimension - 1;

export function canBePromoted(piece, destCoord) {
  if (
    piece.type === 'pawn' &&
    (destCoord.y === minBoardAxis || destCoord.y === maxBoardAxis)
  ) {
    return true;
  }
  return false;
}

export function getPromotionOptions(colour) {
  return [
    constructPiece({ colour, type: 'queen' }),
    constructPiece({ colour, type: 'knight' }),
    constructPiece({ colour, type: 'rook' }),
    constructPiece({ colour, type: 'bishop' })
  ];
}
