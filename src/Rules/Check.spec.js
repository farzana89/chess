import Point from '../Point';
import Bishop from '../Pieces/Bishop';
import King from '../Pieces/King';
import Queen from '../Pieces/Queen';
import Pawn from '../Pieces/Pawn';
import { Check, IsInCheck, MoveWillResultInCheck, CheckMate } from './Check';
import { setupTiles, addPieceToTile } from './TestSetup';

describe('Check entire game state for check', () => {
  it('should check board status and return which king is in check', () => {
    const boardTiles = setupTiles();
    const bishop = new Bishop('black');
    const king = new King('white');

    addPieceToTile(boardTiles, Point(4, 1), bishop);
    addPieceToTile(boardTiles, Point(7, 4), king);

    const result = Check(boardTiles);
    expect(result.isInCheck).toBe(true);
    expect(result.colourInCheck).toBe('white');
  });
});

describe('Move resulting in opponent being in check', () => {
  it('should return true if opposing king is one of the avail attack moves', () => {
    const boardTiles = setupTiles();
    const bishop = new Bishop('black');
    const king = new King('white');

    addPieceToTile(boardTiles, Point(4, 1), bishop);
    addPieceToTile(boardTiles, Point(7, 4), king);

    const result = IsInCheck(boardTiles, bishop, Point(4, 1), 'black');

    expect(result).toBe(true);
  });

  it('should return true if move will result in check without altering destination location', () => {
    const boardTiles = setupTiles();
    const bishop = new Bishop('white');
    const king = new King('black');

    addPieceToTile(boardTiles, Point(0, 4), king);
    const result = MoveWillResultInCheck(
      boardTiles,
      bishop,
      Point(3, 1),
      Point(2, 2)
    );

    expect(result.isInCheck).toBe(true);
    expect(result.colourInCheck).toBe('black');
    expect(boardTiles[2.2].content).toBe('');
  });

  it('should return true if move exposes king to other threats', () => {
    const boardTiles = setupTiles();
    const queen = new Queen('white');
    const king = new King('black');
    const pawn = new Pawn('black');

    addPieceToTile(boardTiles, Point(1, 4), pawn);
    addPieceToTile(boardTiles, Point(3, 6), queen);
    addPieceToTile(boardTiles, Point(0, 3), king);

    const result = MoveWillResultInCheck(
      boardTiles,
      pawn,
      Point(1, 4),
      Point(2, 4)
    );

    expect(result.isInCheck).toBe(true);
    expect(result.colourInCheck).toBe('black');
    expect(boardTiles[2.4].content).toBe('');
  });

  it('should return false if opposing king not in list of avail attack moves', () => {
    const boardTiles = setupTiles();
    const bishop = new Bishop('black');
    const pawn = new Pawn('white');

    addPieceToTile(boardTiles, Point(4, 1), bishop);
    addPieceToTile(boardTiles, Point(6, 3), pawn);

    const result = IsInCheck(boardTiles, bishop, Point(4, 1), 'black');

    expect(result).toBe(false);
  });
});

describe('Check Mate', () => {
  it('Should get check mate, when king is in check and no avail moves will resolve check', () => {
    const boardTiles = setupTiles();

    addPieceToTile(boardTiles, Point(4, 7), new Queen('black'));
    addPieceToTile(boardTiles, Point(7, 4), new King('white'));
    addPieceToTile(boardTiles, Point(7, 3), new Queen('white'));
    addPieceToTile(boardTiles, Point(7, 5), new Bishop('white'));
    addPieceToTile(boardTiles, Point(6, 4), new Pawn('white'));
    addPieceToTile(boardTiles, Point(6, 3), new Pawn('white'));

    const result = CheckMate(boardTiles);

    expect(result).toBe(true);
  });

  it('Should not get check mate when moves that could resolve check are avail', () => {
    const boardTiles = setupTiles();

    addPieceToTile(boardTiles, Point(3, 0), new Bishop('black'));
    addPieceToTile(boardTiles, Point(7, 4), new King('white'));
    addPieceToTile(boardTiles, Point(7, 3), new Queen('white'));
    addPieceToTile(boardTiles, Point(7, 5), new Bishop('white'));
    addPieceToTile(boardTiles, Point(6, 4), new Pawn('white'));
    addPieceToTile(boardTiles, Point(6, 5), new Pawn('white'));

    const result = CheckMate(boardTiles);

    expect(result).toBe(false);
  });
});
