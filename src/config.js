const config = {
  boardDimension: 8,
  xAxisLabels: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'],
  yAxisLabels: [8, 7, 6, 5, 4, 3, 2, 1]
};
export default config;
