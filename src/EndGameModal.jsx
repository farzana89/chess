import React from 'react';
import ReactDOM from 'react-dom';
import { GameContext } from './Game';

const modalRoot = document.getElementById('modal-root');

class EndGameModal extends React.Component {
  constructor() {
    super();
    this.el = document.createElement('div');
  }

  componentDidMount() {
    modalRoot.appendChild(this.el);
  }

  componentWillUnmount() {
    modalRoot.removeChild(this.el);
  }

  render() {
    const modalContent = (
      <GameContext.Consumer>
        {context => {
          if (!context.state.EndGame) return null;
          return (
            <div className="endGameModal">
              <p>Check Mate</p>
            </div>
          );
        }}
      </GameContext.Consumer>
    );
    return ReactDOM.createPortal(modalContent, this.el);
  }
}

export default EndGameModal;
