import React from 'react';
import ReactDOM from 'react-dom';
import { GameContext } from './Game';
import iconFactory from './images/iconFactory';
import { getPromotionOptions } from './Rules/Promotion';

const modalRoot = document.getElementById('modal-root');

class PromotionModal extends React.Component {
  constructor() {
    super();
    this.el = document.createElement('div');
  }

  componentDidMount() {
    modalRoot.appendChild(this.el);
  }

  componentWillUnmount() {
    modalRoot.removeChild(this.el);
  }

  render() {
    const modalContent = (
      <GameContext.Consumer>
        {context => {
          if (!context.state.IsPromoting) return null;
          const pieces = getPromotionOptions(
            context.state.PromotingPiece.piece.colour
          );
          return (
            <div className="promotionOptions">
              {pieces.map((piece, index) => (
                <button
                  className="promotionButton"
                  // eslint-disable-next-line react/no-array-index-key
                  key={`P${index}`}
                  onClick={() => {
                    context.promotePiece(piece);
                  }}
                >
                  {iconFactory(piece)}
                </button>
              ))}
            </div>
          );
        }}
      </GameContext.Consumer>
    );
    return ReactDOM.createPortal(modalContent, this.el);
  }
}

export default PromotionModal;
