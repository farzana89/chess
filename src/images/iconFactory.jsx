import React from 'react';
import PawnIcon from './PawnIcon';
import KnightIcon from './KnightIcon';
import RookIcon from './RookIcon';
import QueenIcon from './QueenIcon';
import KingIcon from './KingIcon';
import BishopIcon from './BishopIcon';

function iconFactory(piece, handleHoverIn, handleHoverOut) {
  const iconProps = {
    className: piece.colour,
    onMouseEnter: handleHoverIn,
    onMouseLeave: handleHoverOut
  };

  if (piece.type === 'pawn') {
    return <PawnIcon {...iconProps} />;
  }
  if (piece.type === 'knight') {
    return <KnightIcon {...iconProps} />;
  }
  if (piece.type === 'bishop') {
    return <BishopIcon {...iconProps} />;
  }
  if (piece.type === 'rook') {
    return <RookIcon {...iconProps} />;
  }
  if (piece.type === 'queen') {
    return <QueenIcon {...iconProps} />;
  }
  if (piece.type === 'king') {
    return <KingIcon {...iconProps} />;
  }
  throw new Error(`Invalid type ${piece.type}`);
}

export default iconFactory;
