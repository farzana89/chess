import Pawn from './Pieces/Pawn';
import Knight from './Pieces/Knight';
import Bishop from './Pieces/Bishop';
import Rook from './Pieces/Rook';
import Queen from './Pieces/Queen';
import King from './Pieces/King';
import constructPiece from './PieceFactory';

describe('Chess piece contruction', () => {
  it('Creates an instance of a black Pawn piece', () => {
    const piece = constructPiece({ type: 'pawn', colour: 'black' });
    expect(piece).toBeInstanceOf(Pawn);
    expect(piece.colour).toEqual('black');
  });

  it('Creates an instance of a black Knight piece', () => {
    const piece = constructPiece({ type: 'knight', colour: 'black' });
    expect(piece).toBeInstanceOf(Knight);
    expect(piece.colour).toEqual('black');
  });

  it('Creates an instance of a white Bishop piece', () => {
    const piece = constructPiece({ type: 'bishop', colour: 'white' });
    expect(piece).toBeInstanceOf(Bishop);
    expect(piece.colour).toEqual('white');
  });

  it('Creates an instance of a white Rook piece', () => {
    const piece = constructPiece({ type: 'rook', colour: 'white' });
    expect(piece).toBeInstanceOf(Rook);
    expect(piece.colour).toEqual('white');
  });

  it('Creates an instance of a black Queen piece', () => {
    const piece = constructPiece({ type: 'queen', colour: 'black' });
    expect(piece).toBeInstanceOf(Queen);
    expect(piece.colour).toEqual('black');
  });

  it('Creates an instance of a black King piece', () => {
    const piece = constructPiece({ type: 'king', colour: 'black' });
    expect(piece).toBeInstanceOf(King);
    expect(piece.colour).toEqual('black');
  });

  it('Throw invalid type exception for invalid chess piece', () => {
    expect(() =>
      constructPiece({ type: 'thing', colour: 'black' })
    ).toThrowError('Invalid type thing');
  });
});
