import React from 'react';
import Board from './Board';
import Desk from './Desk';
import GameLog from './GameLog';
import PromotionModal from './PromotionModal';
import EndGameModal from './EndGameModal';
import { GameProvider } from './Game';
import './app.css';

function App() {
  return (
    <GameProvider>
      <Board />
      <Desk />
      <GameLog />
      <PromotionModal />
      <EndGameModal />
    </GameProvider>
  );
}

export default App;
