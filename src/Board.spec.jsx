import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';
import Point from './Point';
import Board from './Board';
import Piece from './Piece';
import constructPiece from './PieceFactory';
import { context, setState } from './__mocks__/contextSetup';
import config from './config';

function tiles() {
  const boardTiles = {};
  for (let y = 0; y < config.boardDimension; y++) {
    for (let x = 0; x < config.boardDimension; x++) {
      const key = `${x}.${y}`;
      boardTiles[key] = { coord: Point(x, y), content: '' };
    }
  }
  boardTiles[0.1] = {
    coord: Point(0, 1),
    content: constructPiece({ colour: 'black', type: 'pawn' })
  };
  return boardTiles;
}

const state = {
  turn: 'black',
  tiles: tiles()
};

setState(state);

jest.mock('./Game');

describe('Board tests', () => {
  it('Renders an 8x8 grid', () => {
    const board = ReactTestUtils.renderIntoDocument(<Board />);

    const rows = ReactTestUtils.scryRenderedDOMComponentsWithClass(
      board,
      'board-row'
    );
    const boardTiles = ReactTestUtils.scryRenderedDOMComponentsWithClass(
      board,
      'tile'
    );

    expect(rows.length).toBe(8);
    expect(boardTiles.length).toBe(64);
  });
});

describe('Board events', () => {
  const board = ReactTestUtils.renderIntoDocument(<Board />);
  const pieces = ReactTestUtils.scryRenderedComponentsWithType(board, Piece);
  const piece = pieces[0];

  it('Hover in event', () => {
    const icon = ReactTestUtils.findRenderedDOMComponentWithTag(piece, 'svg');

    ReactTestUtils.Simulate.mouseEnter(icon);

    expect(context.handleHoverIn).toHaveBeenCalled();
  });

  it('Hover out event', () => {
    const icon = ReactTestUtils.findRenderedDOMComponentWithTag(piece, 'svg');

    ReactTestUtils.Simulate.mouseLeave(icon);

    expect(context.handleHoverOut).toHaveBeenCalled();
  });

  // TODO: Figure out how to hook into react-dnd drop end event
  xit('move piece event', () => {
    const icon = ReactTestUtils.findRenderedDOMComponentWithTag(piece, 'svg');

    ReactTestUtils.Simulate.dragEnd(icon);

    expect(context.movePiece).toHaveBeenCalled();
  });
});
