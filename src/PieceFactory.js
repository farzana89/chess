import Pawn from './Pieces/Pawn';
import Knight from './Pieces/Knight';
import Bishop from './Pieces/Bishop';
import Rook from './Pieces/Rook';
import Queen from './Pieces/Queen';
import King from './Pieces/King';

function constructPiece({ type, colour }) {
  if (type === 'pawn') {
    return new Pawn(colour);
  }
  if (type === 'knight') {
    return new Knight(colour);
  }
  if (type === 'bishop') {
    return new Bishop(colour);
  }
  if (type === 'rook') {
    return new Rook(colour);
  }
  if (type === 'queen') {
    return new Queen(colour);
  }
  if (type === 'king') {
    return new King(colour);
  }
  throw new Error(`Invalid type ${type}`);
}

export default constructPiece;
